#fizz_buzz is Python program which iterates the integers from 1 to 50


#for numbers which are multiples of both three and five program prints "FizzBuzz"
for fizz_buzz in range(50):
    if fizz_buzz % 3 == 0 and fizz_buzz % 5 == 0:
        print('FizzBuzz')
        continue
    
#for multiples of three program prints "Fizz" instead of the number
    elif fizz_buzz % 3 == 0:
        print('Fizz')
        continue
        
#for the multiples of five prints "Buzz"
    elif fizz_buzz % 5 == 0:
        print('Buzz')
        continue
    print(fizz_buzz)
